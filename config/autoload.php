<?php
require_once('../helpers/helper.php');

\api\helpers\helper::include_all_php("../config");
\api\helpers\helper::include_all_php("../controllers");
\api\helpers\helper::include_all_php("../models");
\api\helpers\helper::include_all_php("../services");
\api\helpers\helper::include_all_php("../helpers");
