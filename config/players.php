<?php
namespace api\config;

class players{

    public $player1;
    public $player2;

    public function __construct()
    {
        $this->player1 = $this->initPlayer1();
        $this->player2 = $this->initPlayer2();
    }

    private function initPlayer1(){

        /*
         * Orderus config
         */
        return [
            'name'  => 'Orderus',
            'class' => '\api\models\Hero',
            'stats' => [
                'health'    => new \api\models\Stats('health',  \api\helpers\helper::pseudoRandom([70,100])),
                'strength'  => new \api\models\Stats('strength',\api\helpers\helper::pseudoRandom([70,80])),
                'defence'   => new \api\models\Stats('defence', \api\helpers\helper::pseudoRandom([45,55])),
                'speed'     => new \api\models\Stats('speed',   \api\helpers\helper::pseudoRandom([40,50])),
                'luck'      => new \api\models\Stats('luck',    \api\helpers\helper::pseudoRandom([10,30]))
            ],
            'skills' => [
                'RapidStrike' => new \api\models\RapidStrike(10),
                'MagicShield' => new \api\models\MagicShield(20,50/100)
            ]
        ];

    }


    private function initPlayer2(){

        /*
        * Wild Beast Config
        */
        return [
            'name'  => 'Wild Beast',
            'class' => '\api\models\Monster',
            'stats' => [
                'health'    => new \api\models\Stats('health',  \api\helpers\helper::pseudoRandom([60,90])),
                'strength'  => new \api\models\Stats('strength',\api\helpers\helper::pseudoRandom([60,90])),
                'defence'   => new \api\models\Stats('defence', \api\helpers\helper::pseudoRandom([40,60])),
                'speed'     => new \api\models\Stats('speed',   \api\helpers\helper::pseudoRandom([40,60])),
                'luck'      => new \api\models\Stats('luck',    \api\helpers\helper::pseudoRandom([25,40]))
            ]
        ];

    }

}
