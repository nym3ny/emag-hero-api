<?php
namespace api\controllers;

class Controller{

    public function afterAction($result){

        header('Content-Type: application/json');
        echo json_encode($result);
    }

}