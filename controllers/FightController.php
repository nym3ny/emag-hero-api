<?php
namespace api\controllers;

class FightController extends Controller{

    private $player1;
    private $player2;

    public function __construct()
    {
        $playersConfig = new \api\config\players();

        $this->player1    = new $playersConfig->player1['class']($playersConfig->player1);
        $this->player2    = new $playersConfig->player2['class']($playersConfig->player2);
    }

    public function indexAction(){

        $fightService = new \api\services\FightService($this->player1,$this->player2);
        $fightService->startFight();

        return $fightService->getResult();
    }

}