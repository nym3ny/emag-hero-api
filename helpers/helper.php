<?php
namespace api\helpers;

class helper{

    static function include_all_php($folder){
        foreach (glob("{$folder}/*.php") as $filename)
        {
            require_once($filename);
        }
    }

    static function pseudoRandom($range){

        return mt_rand($range[0],$range[1]);

    }

    static function getShortName($class_name){

        $class =  new \ReflectionClass($class_name);
        return $class->getShortName();

    }

}