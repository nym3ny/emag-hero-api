<?php
namespace api\helpers;

class route{
    private $controller;
    private $action;

    public function __construct(){

        if (isset($_GET['r'])) {

            $route = explode('/',$_GET['r']);
            $this->controller = $route[0];
            $this->action     = @$route[1];
            if($this->action ==''){
                $this->action = 'index';
            }

        } else {

            $this->controller = 'fight';
            $this->action     = 'index';
        }

    }

    private function call($controllerClass, $action) {

        $controller = new $controllerClass;
        return $controller->{$action}();

    }

    public function run(){

        $controllerClass = '\\api\\controllers\\'.$this->controller.'Controller';
        $actionMethod = $this->action.'Action';

        if(class_exists($controllerClass)){

            $result = $this->call($controllerClass, $actionMethod);
            $controller = new $controllerClass;
            return $controller->afterAction($result);

        }else{

            throw new \Exception("Cant't find $controllerClass.");

        }

    }

}