<?php
namespace api\models;

abstract class Character{

    public $stats = [];
    public $name;

    public function __construct($characterConfig){

        $this->stats = array_merge($this->stats, $characterConfig['stats']);
        $this->name = $characterConfig['name'];

    }

}