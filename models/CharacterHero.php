<?php
namespace api\models;

class Hero extends Character{

    public $skills = [];

    public function __construct($characterConfig){

        parent::__construct($characterConfig);
        $this->skills = array_merge($this->skills, $characterConfig['skills']);

    }

}