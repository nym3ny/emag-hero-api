<?php
namespace api\models;

abstract class Skill{

    public $rate;
    public $purpose;
    public $name = "Unknown Skill";

    public function __construct($rate){

        $this->rate = $rate;

    }

}