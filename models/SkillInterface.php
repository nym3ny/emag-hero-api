<?php
namespace api\models;

interface SkillInterface{

    public function execute(\api\services\FightService $fightService);
}