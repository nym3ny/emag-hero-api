<?php
namespace api\models;

class MagicShield extends Skill implements SkillInterface {

    public $value;
    public $name = "MagicShield";

    public function __construct($rate,$value){

        parent::__construct($rate);
        $this->value = $value;
        $this->purpose = 'defend';
    }

    public function execute(\api\services\FightService $fightService){

        $round_damage = $fightService->getProperty('round_damage');


        if(\api\helpers\helper::pseudoRandom([0,100]) > $this->rate) {

            $damage = (int)($round_damage * $this->value);
            $fightService->setProperty('round_damage',$damage);

            $round_skills = $fightService->getProperty('round_skills');
            $round_skills[] = $this->name;
            $fightService->setProperty('round_skills',$round_skills);
        }
    }

}