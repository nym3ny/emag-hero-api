<?php
namespace api\models;

class RapidStrike extends Skill implements SkillInterface{

    public $name = "RapidStrike";
    public function __construct($rate){

        parent::__construct($rate);
        $this->purpose = 'attack';
    }

    public function execute(\api\services\FightService $fightService){

        $player = $fightService->getProperty('player');
        $round_damage = $fightService->getProperty('round_damage');

        if(\api\helpers\helper::pseudoRandom([0,100]) > $this->rate) {

            $damage = $player[1]->attack($player[2]->model());
            $fightService->setProperty('round_damage',$round_damage + $damage);

            $round_skills = $fightService->getProperty('round_skills');
            $round_skills[] = $this->name;
            $fightService->setProperty('round_skills',$round_skills);
        }

    }
}