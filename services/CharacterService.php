<?php
namespace api\services;

abstract class CharacterService{

    public $character;

    public function __construct(\api\models\Character $character)
    {

        $this->character = $character;

    }

    public function Stats($stats_name){

        if(isset($this->character->stats[$stats_name])){

            return $this->character->stats[$stats_name];

        }else{
            return new \api\models\Stats($stats_name,0);
        }


    }

    public function model(){

        return $this->character;

    }

    public function attack(\api\models\Character $defender){

        $defender_class = '\\api\\services\\' . \api\helpers\helper::getShortName($defender).'Service';

        $defenderService = new $defender_class($defender);

        /*
         * If opponent has luck
         */
        if(\api\helpers\helper::pseudoRandom([0,100]) > $defenderService->Stats('luck')->value){

            $damage  = $this->Stats('strength')->value  - $defenderService->Stats('defence')->value;

        }else{
            $damage = 0;
        }

        if($damage < 0){
            $damage = 0;
        }

        return $damage;
    }

    public function defend($damage){

        $health = $this->Stats('health')->value - $damage;
        if($health < 0){
            $health = 0;
        }
        return $health;

    }

    public function getStatsReport(){

        $report = [];
        foreach($this->character->stats as $stats){

            $report[$stats->name] = $stats->value;

        }
        return $report;
    }

    public function getSkillsReport(){

        $report = [];
        if(isset($this->character->skills)){
            foreach($this->character->skills as $skill){

                $report[$skill->name] = $skill->rate;

            }
        }

        return $report;
    }

}