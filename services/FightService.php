<?php
namespace api\services;

class FightService{

    private $player;
    private $result = [];
    private $round_damage;
    private $round_number = 0;
    private $round_block_damage = 0;
    private $round_initial_damage = 0;
    private $round_enchanted_damage = 0;
    private $round_skills = [];

    public function __construct(\api\models\Character $player1, \api\models\Character $player2){

        $player1Service = '\\api\\services\\' . \api\helpers\helper::getShortName($player1).'Service';
        $player2Service = '\\api\\services\\' . \api\helpers\helper::getShortName($player2).'Service';

        $this->player[1] = new $player1Service($player1);
        $this->player[2] = new $player2Service($player2);

    }

    public function startFight(){

        $this->prepareToFight();

        for($i = 1; $i <= 25 &&
        $this->player[2]->Stats('health')->value > 0 &&
        $this->player[1]->Stats('health')->value > 0; $i++){

            $this->initRound();
            $this->round_number = $i;
            $this->round_damage = $this->player[1]->attack($this->player[2]->model());
            $this->round_initial_damage = $this->round_damage;
            $this->applySkills();
            $this->calculateSkillsEfficiency();
            $this->player[2]->Stats('health')->value = $this->player[2]->defend($this->round_damage);
            $this->registerRound();
            $this->rotatePlayers();

        }

        $this->registerWinner();

    }
    private function initRound(){

        $this->round_damage;
        $this->round_number = 0;
        $this->round_block_damage = 0;
        $this->round_initial_damage = 0;
        $this->round_enchanted_damage = 0;
        $this->round_skills = [];

    }

    public function setProperty($property,$value){

        $this->{$property} = $value;
    }

    public function getProperty($property){

        return $this->{$property};
    }

    /*
     * Apply skills to damage
     */
    private function applySkills(){

        $this->round_skills = [];

        if(isset($this->player[1]->model()->skills)){
            foreach($this->player[1]->model()->skills as $skill){

                if($skill->purpose == 'attack'){
                    $skill->execute($this);
                }

            }
        }

        if(isset($this->player[2]->model()->skills)){
            foreach($this->player[2]->model()->skills as $skill){

                if($skill->purpose == 'defend'){
                    $skill->execute($this);
                }

            }
        }

    }

    //Calculate damage and blocking after everything apply
    private function calculateSkillsEfficiency(){

        if($this->round_damage <  $this->round_initial_damage){
            $this->round_block_damage = $this->round_initial_damage - $this->round_damage;
        }else{
            $this->round_block_damage = 0;
            $this->round_enchanted_damage = $this->round_damage - $this->round_initial_damage;
        }
    }


    /*
     * Set the attacker to position 1 in array
     */
    private function prepareToFight(){

        if($this->player[1]->Stats('speed')->value < $this->player[2]->Stats('speed')->value ||
            ($this->player[1]->Stats('speed')->value == $this->player[2]->Stats('speed')->value &&
                $this->player[1]->Stats('luck')->value < $this->player[2]->Stats('luck')->value)){

            $this->rotatePlayers();
        }

        $this->registerRound();

    }

    /*
     * rotate players in array
     */
    private function rotatePlayers(){

        $player = $this->player[1];
        $this->player[1] = $this->player[2];
        $this->player[2] = $player;

    }

    private function registerRound(){

        if(!$this->round_number){
            $this->result['players'] =
                [
                    1 =>  ['name'   => $this->player[1]->model()->name,
                           'stats'  => $this->player[1]->getStatsReport(),
                           'skills' => $this->player[1]->getSkillsReport()],

                    2 =>  ['name'   => $this->player[2]->model()->name,
                           'stats'  => $this->player[2]->getStatsReport(),
                           'skills' => $this->player[2]->getSkillsReport()]
                ];
        }else{
            $this->result['rounds'][] =
                ['name' => 'Round ' . $this->round_number,
                    'fight' => [
                        'initial_damage' => $this->round_initial_damage,
                        'enchanted_damage' => $this->round_enchanted_damage,
                        'blocked_damage' => $this->round_block_damage,
                        'sent_damage' => $this->round_damage,
                    ],
                    'skills' => $this->round_skills,
                    'players' => [
                        1 =>  ['name'   => $this->player[1]->model()->name,
                               'stats'  => $this->player[1]->getStatsReport()],
                        2 =>  ['name'   => $this->player[2]->model()->name,
                               'stats'  => $this->player[2]->getStatsReport()]
                    ],


                ];
        }

    }

    public function registerWinner(){

        if($this->player[1]->Stats('health')->value >  $this->player[2]->Stats('health')->value){
            $this->result['winner'] = $this->player[1]->model()->name;
        }else{
            $this->result['winner'] = $this->player[2]->model()->name;
        }

    }

    public function getResult(){

       return $this->result;

    }

}