<?php
namespace api\services;

class HeroService extends CharacterService{


    public function hasSkill($skill_name){

        if($this->character->skills[$skill_name]){
            return true;
        }else{
            return false;
        }

    }

    public function Skill($skill_name){

        if($this->character->skills[$skill_name]){

            return $this->character->skills[$skill_name];

        }else{
            return false;
        }

    }

}