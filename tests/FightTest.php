<?php
namespace api\tests;

class FightTest extends \PHPUnit\Framework\TestCase{

    public function __construct()
    {
        parent::__construct();
        require_once('../config/autoload.php');
    }

    public function testOrderusWin()
    {
        /*
         * Orderus config
         */
        $orderusConfig = [
            'name'  => 'Orderus',
            'stats' => [
                'health'    => new \api\models\Stats('health',  100),
                'strength'  => new \api\models\Stats('strength',80),
                'defence'   => new \api\models\Stats('defence', 55),
                'speed'     => new \api\models\Stats('speed',   50),
                'luck'      => new \api\models\Stats('luck',    30)
            ],
            'skills' => [
                'RapidStrike' => new \api\models\RapidStrike(10),
                'MagicShield' => new \api\models\MagicShield(20,50/100)
            ]
        ];

        /*
         * Wild Beast Config
         */
        $wildBeastConfig = [
            'name'  => 'Wild Beast',
            'stats' => [
                'health'    => new \api\models\Stats('health',  60),
                'strength'  => new \api\models\Stats('strength',60),
                'defence'   => new \api\models\Stats('defence', 40),
                'speed'     => new \api\models\Stats('speed',   40),
                'luck'      => new \api\models\Stats('luck',    25)
            ]
        ];

        $hero       = new \api\models\Hero($orderusConfig);
        $monster    = new \api\models\Monster($wildBeastConfig);

        $fightService = new \api\services\FightService($hero,$monster);
        $fightService->startFight();
        $result = $fightService->getResult();


        $this->assertEquals($result['winner'], 'Orderus');
    }

    public function testWildBeastWin(){

        /*
         * Orderus config
         */
        $orderusConfig = [
            'name'  => 'Orderus',
            'stats' => [
                'health'    => new \api\models\Stats('health',  70),
                'strength'  => new \api\models\Stats('strength',70),
                'defence'   => new \api\models\Stats('defence', 45),
                'speed'     => new \api\models\Stats('speed',   40),
                'luck'      => new \api\models\Stats('luck',    10)
            ],
            'skills' => [
                'RapidStrike' => new \api\models\RapidStrike(10),
                'MagicShield' => new \api\models\MagicShield(20,50/100)
            ]
        ];

        /*
         * Wild Beast Config
         */
        $wildBeastConfig = [
            'name'  => 'Wild Beast',
            'stats' => [
                'health'    => new \api\models\Stats('health',  90),
                'strength'  => new \api\models\Stats('strength',90),
                'defence'   => new \api\models\Stats('defence', 60),
                'speed'     => new \api\models\Stats('speed',   60),
                'luck'      => new \api\models\Stats('luck',    40)
            ]
        ];

        $hero       = new \api\models\Hero($orderusConfig);
        $monster    = new \api\models\Monster($wildBeastConfig);

        $fightService = new \api\services\FightService($hero,$monster);
        $fightService->startFight();
        $result = $fightService->getResult();

        $this->assertEquals($result['winner'], 'Wild Beast');
    }

    public function testFairFight(){
        //verify if player1 and player2 have assigned same type of stats

    }

    public function testPlayersSkillsExist(){
        //verify if the players skills (classes) exist

    }

    public function testMinimalPlayersConfig(){
        //verify the minimal players config required: stats, skills etc...

    }

    public function testReturnExpectedStructure(){
        //verify if return expected json structure for Web interface

    }

}