<?php
require_once('../config/autoload.php');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: TRUE");
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, OPTIONS, DELETE');

$route = new \api\helpers\route();
try{
    $route->run();
}
catch(Exception $e){
    echo $e->getMessage();
}

